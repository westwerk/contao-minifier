#Contao minifier
using `matthiasmullie/minify` (special thanks) and contao's combiner to minify `.js` and `.css` files after the combine 
process 

- simply install it with composer `composer require westwerk/contao-minifier`
- visit the install tool from contao `yourpage.com/contao/install` and update the database
- in your page layout setting you can choose the minifier