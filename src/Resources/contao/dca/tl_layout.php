<?php
// dca/tl_content.php
/**
 * Table tl_content
 */
$strName = 'tl_layout';
/* Palettes */
$GLOBALS['TL_DCA'][$strName]['palettes']['default'] = str_replace
(
    'combineScripts',
    'combineScripts,minifyScripts',
    $GLOBALS['TL_DCA'][$strName]['palettes']['default']
);
$GLOBALS['TL_DCA'][$strName]['fields']['minifyScripts'] = array
(
    'label' => &$GLOBALS['TL_LANG']['ww']['minifyScripts'],
    'exclude' => true,
    'search' => true,
    'inputType' => 'checkbox',
    'eval' => array(
        'tl_class' => 'w50 m12',

    ),
    'sql' => "CHAR default 0"
);