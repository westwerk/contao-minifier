<?php

namespace Westwerk\ContaoMinifier;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use MatthiasMullie\Minify;

class ContaoMinifier extends Bundle
{
    public function minifyCombinedFile($strContent, $strKey, $strMode)
    {
        $page = \LayoutModel::findById($GLOBALS['objPage']->layout);
        if ($page->minifyScripts) {
            if ($strMode == '.css') {
                $this->minifier = new Minify\CSS();
                $this->minifier->add($strContent);
                $strContent = $this->minifier->minify();
            }
            if ($strMode == '.js ') {
                $this->minifier = new Minify\JS();
                $this->minifier->add($strContent);
                $strContent = $this->minifier->minify();
            }
        }
        return $strContent;
    }
}
